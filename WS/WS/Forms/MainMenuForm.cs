﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WS.DAO;

namespace WS.Forms
{
    public partial class MainMenuForm : BaseForm
    {

        public string login { get; set; }
        public MainMenuForm()
        {
            InitializeComponent();
            userName.Text = LoginController.GetInstance().user.login;
        }

        private void listButton_Click(object sender, EventArgs e)
        {
            var listForm = new ListForm();
            showNextForm(listForm, false);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tablesButton_Click(object sender, EventArgs e)
        {
            var tableForm = new TableForm();
            showNextForm(tableForm, false);
        }
    }
}
