﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WS.DAO;
using WS.Forms;
using WS.Models;

namespace WS.Forms
{
    public partial class TableForm : BaseForm
    {
        public TableForm()
        {
            InitializeComponent();
            DataTable dt = new DataTable();
            dt.Columns.Add("Last Name");
            dt.Columns.Add("First Name");
            dt.Columns.Add("Role");
            dt.Columns.Add("Avatar", typeof(Bitmap));
            var users = UserDao.GetUsers();
            foreach (User u in users)
            {
                DataRow r = dt.NewRow();
                r["Last Name"] = u.lastName;
                r["First Name"] = u.lastName;
                r["Role"] = u.role;
                r["Avatar"] = UserDao.GetImageFromFile(u.photo);
                dt.Rows.Add(r);
            }
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
